# Cookiecutters for new and existing research software projects

This is the repository holding the materials associated with the SeptembRSE
Walkthrough/Demo [Cookiecutters for new and existing research software
projects](https://septembrse.github.io/#/event/W1006). Please see that event
page for the abstract and description of the event, or look at [the submission
form](./walkthrough_submission/submission_form.md).

The materials for performing the demonstration itself are in the `demo-space/`
folder. These consist of primarily of a [`Dockerfile`](./demo-space/Dockerfile)
used to produce a suitable container in which to perform the demo and an
associated [script](./demo-space/mount/demo_script.sh) with all of the commands
executed from the command line during the demo.

The recording of the walkthrough/demo is on [YouTube](https://youtu.be/iKL0UFUsWSY?t=3717)

## Usage

The demo is run from a Docker container that can be spun up as follows. You are
required to have a working installation of Docker on your system.

```bash
cd demo-space
./docker-cmd.sh build
./docker-cmd.sh bash
```

This will drop you into a bash shell in the container. The bash commands for
demonstrating the use of the cookiecutter are located in `~/demo/demo_script.sh`
in the container. In principle, you can get to the "end state" of the demo by
running the script, but you will need to modify the GitLab user for some of the
commands.

## Presentation Set Up

To run the demo, it is best to have two screens.

### Screen 1 (not shared)

- [Demo script](./demo-space/mount/demo_script.sh)

### Screen 2 (shared with audience)

- Terminal running the Docker container according to the [Usage](#usage) section. Make sure it is centred and with nice big font.
- Web browser window with <https://gitlab.com> open, and
  <https://ukaea.github.io/scientific-python-cookiecutter/>. Minimized
  initially.
- Slide deck.

### Other

- Make sure there isn't a project named "Example" in the GitLab account you plan to use.

