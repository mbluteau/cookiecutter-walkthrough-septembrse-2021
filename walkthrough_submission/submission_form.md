## Title

Cookiecutters for new and existing research software projects

## Abstract

It is well-known that good software engineering practices in research software
lag well behind the standard in industry or commercial enterprise. The reasons
for this are complex, but two contributing factors are lack of awareness and
time. [Cookiecutter](https://cookiecutter.readthedocs.io/en/stable/), a
popular command-line utility for generating project content from templates, can
help partially alleviate these issues by offloading decisions about project
structure and tools from the researcher. This walkthrough will demonstrate two
use cases of cookiecutter from the bash shell:

1. Starting a scientific software project in Python from scratch
2. Migrating an existing project into a cookiecutter-generated skeleton while
   retaining git history.

The cookiecutter used for these examples had been modified from 
[the Scientific Python Cookiecutter
Template](https://nsls-ii.github.io/scientific-python-cookiecutter/index.html).
Although this walkthrough uses Python, the principles are equally
applicable to other programming languages for which there are cookiecutters
readily available.

## Audience

This walkthrough will be of interest to anyone involved in writing research
software who is looking to improve the development practices in their new or
existing projects in any language. Experience with the bash shell and git is
assumed, and while familiarity with Python and pip will be helpful, it is not
mandatory. No knowledge of cookiecutter is expected.

## Outcomes

Attendees will gain knowledge about how to use cookiecutter for new and
existing projects and understand the benefits that project templates offer.
This outcome will be supported by releasing the recording, slides, and
reference material associated with this walkthrough via the appropriate
SeptembRSE channels. A blog post specifically on migrating projects into a
cookiecutter-generated skeleton is also envisioned, and will be disseminated
with the above.


## Accessibility

As stated above, the slides and recording will be made available in accordance
with the SeptembRSE policy and licence. The accessibility guidelines will also
be consulted during development of the walkthrough material, with particular
care taken selecting font size and colors of the presenter's terminal program.

## Promotion

The target audience is RSEs and any researcheers who code. Twitter is the
predominant social media platform for this demographic, so promotion will be
focussed there. The authors personal Twitter account and the RSE Culham
account will be used to advertise the talk. In addition, the UK-RSE Slack will
be another useful location for promotion.

## Technological Requirements

Some way of collecting audience questions would be beneficial. Sli.do or Zoom's
built-in Q&A should be sufficient. 
