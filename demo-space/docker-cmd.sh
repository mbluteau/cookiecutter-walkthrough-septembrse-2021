#!/bin/bash

# Names to identify images and containers of this app
IMAGE_NAME='cookiecutter-walkthrough'
CONTAINER_NAME='cookiecutter-walkthrough'

log() {
  echo "$BLUE > $1 $NORMAL"
}

error() {
  echo ""
  echo "$RED >>> ERROR - $1$NORMAL"
}

build() {
  docker build -t $IMAGE_NAME .

  [ $? != 0 ] && \
    error "Docker image build failed !" && exit 100
}

bash() {
  log "BASH"
  docker run -it --rm -v $(pwd)/mount:/home/mbluteau/demo \
  -v ~/.ssh:/home/mbluteau/.ssh \
  $IMAGE_NAME /bin/bash
}

stop() {
  docker stop $CONTAINER_NAME
}

start() {
  docker start $CONTAINER_NAME
}

remove() {
  log "Removing previous container $CONTAINER_NAME" && \
      docker rm -f $CONTAINER_NAME &> /dev/null || true
}

# This allows one of the above functions to be called if pass as an argument to
# the script
$*