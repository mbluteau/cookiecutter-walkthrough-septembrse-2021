FROM ubuntu:20.04

# Environment
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London

# Install dependencies
RUN apt-get update && apt-get -y install \
    gcc \
    git \
    python3 \
    python3-dev \
    python3-distutils \
    python3-pip \
    python3-venv \
    python-is-python3 \
    tree \
    vim

# Create a non-root user for the container
ARG USERNAME=mbluteau
ARG USER_UID=1002
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # Add sudo support.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# Set this as the default user
USER $USERNAME
WORKDIR /home/mbluteau

# Get custom bash environment with vi keybindings and a simple PS1
COPY --chown=mbluteau:mbluteau .bashrc /home/mbluteau/

# Configure git
# TODO should this be assumed? Or good to do this during the demo explicitly?
RUN git config --global user.email "matthew.bluteau@ukaea.com" \
  && git config --global user.name "mbluteau"