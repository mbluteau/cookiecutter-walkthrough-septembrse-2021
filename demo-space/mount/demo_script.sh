#!/bin/bash

# Verify that the correct dependencies are present
python --version
pip --version
git --version

# Install cookiecutter in user space
pip install --user --upgrade cookiecutter
# This produces a warning about the cookiecutter script being placed outside of
# the current path. There are two options to "fix" this.
# 1.
export PATH=/home/mbluteau/.local/bin:$PATH
# or 2. always prepend the cookiecutter command with `python -m`
# I am going with option 1)

# Now, download and use the cookiecutter template
cookiecutter https://github.com/ukaea/scientific-python-cookiecutter <<EOF
Matthew Bluteau
matthew.bluteau@ukaea.uk
gitlab.com
mbluteau
example





1
EOF
# The various names prompted for can be a bit confusing. These are the explanations
#
# 1. project_name – Human-friendly title that will show up in your README. Case
# sensitive. Spaces allowed.
#
# 2. package_dist_name – The name to use when you pip install ___. Dashes and
# underscores are allowed. Dashes are conventional. Case insensitive. This
# needs to be checked against name clashing with an existing package in PyPI
# because the cookiecutter does not do that automatically.
#
# 3. package_dir_name — The name to use when you import ___ in Python.
# Underscores are the only punctuation allowed. Conventionally lowercase. Case
# sensitive depending on system. This will also be the name of the subdirectory
# in which the package source code is contained.
#
# 4. repo_name — The name of the GitHub repository. This will be the name of
# the new directory on your filesystem.

# Let's take a quick look at the structure of what we have
tree -a example/
# At the top level, there are a number of configuration files, some of which we
# will go through quickly
# Then, there is the `docs/` which contains the documentation for the project
# Finally, we have the `example/` folder which holds the source code

# Okay, let's dive into our project directory
cd example/

# The first thing we want to do here is create a virtual environment to sandbox
# the dependencies of our project. This is common practice in Python
# development, and there is a whole lot of material on it that I am not going to
# go into. There is/was a whole event on Python environment management in
# SeptembRSE TODO link!
# It is conventional and good practice to put your virtual environment directory
# within the project directory to keep them close together and remind you that
# the virtual environment is required.
python -m venv venv
# The last argument is the directory of the venv that will be created.

# Activate the virtual-env
. venv/bin/activate
# Notice that our prompt is now prepended with the name of the virtual environment

# Initialise a git directory to track all of the changes we will make
git init
# And then commit all these initial files so that we can roll back to the very beginning if we need to.
git add .
git status
# Notice that venv not added
git commit -m "Initial commit"
# You might get prompted to configure git at this point if you haven't already
# Change the name of the primary branch
git branch -M main

# Create a project on GitLab or GitHub; I will be using GitLab for this
# demonstration. Steps:
# 1. Navigate to gitlab.com and log in
# 2. Select the '+' button and 'New project/repository'
# 3. Fill in information:
#    Project name = Example
#    Project slug = example
#    Project description = (leave blank)
#    Visibility level = Public
#    Uncheck "Initialise repo with README"
#
# Add the remote repository; note that I have ssh keys set up
git remote add origin git@gitlab.com:mbluteau/example.git
# And then push to it
git push -u origin main

# Finally, install the package in "editable" mode, meaning any updates to the
# source files will be available immediately when we use the package during
# development.
pip install -e .
# And install developer requirements, which are needed when developing the package, but not when using it
pip install --upgrade -r requirements-dev.txt

# While waiting for those to install, navigate to the tutorial associated with
# the cookiecutter: https://ukaea.github.io/scientific-python-cookiecutter/

# Check that our package has been installed properly
python <<EOF
import example
quit()
EOF

# This is really the bulk of the work getting the template set up. Now, let's
# get a taste of what we actually get from the template.

# First, we need to add some actual source code to the project. Copy some
# representative files from the demo folder:
cp ~/demo/refraction.py example/
cp ~/demo/test_examples.py example/tests

# Remember to regularly commit your changes to version control once you have
# completed a suitable unit of work. This is loosely the order that one should
# do this: add some source code and tests for that source, then run tests, then
# commit work if tests pass. I am not doing this here to allow for the CI
# pipeline to run so it is ready for the end of the demo.
git add example/refraction.py example/tests/test_examples.py
git commit -m "Add Snell's law functionality"
# And also regularly push and pull, especially if you are working on the same
# branch as someone else.
git pull
git push

# As a reminder, it is the example/ subdirectory we want to put these files in.
# Have a look at the structure of the directories again:
tree -d -L 2 ../example

# Take a look at the first source file provided:
# $ vim example/refraction.py
#
# Some key points about this source file:
# - It is being placed in the example/ subfolder next to the __init__.py file
#   and tests/ folder. This placement is essential so that our package has
#   access and is "aware" of this source file.
# - It is performing a simple calculation with Snell's Law to obtain the angle
#   of refraction of light. There is a nice docstring explaining this, and the
#   tutorial takes the user through good practices for writing dosctrings and
#   documentation in general, something that is outside the scope of this
#   walkthrough.
# - There is a dependency on numpy, so we need to add this to the
#   `requirements.txt` file. 
echo "numpy" >> requirements.txt
# 
# And now look at the other file that has been added:
# $ vim example/tests/test_examples.py
#
# Some points:
# - This file contains the tests for the refraction.py source file written using
#   the `pytest` framework.
# - The template is configured for `pytest`, but any testing framework should be
#   easily dropped in place since not much configuration has been done.
# - The tutorial provides some guidance on writing tests like these, but again
#   this is outside the current scope.

# Let's see that we can run the tests. 
pytest
# Great, the two tests are passing. When developing this package further, all we
# need to do is add source code to refraction.py and then corresponding test
# code in test_examples.py. The results will be picked up and displayed by
# pytest.

# We also can easily get test coverage reports with the `coverage` tool
coverage run -m pytest && coverage report -m

# Another nice feature we get with the template is automatic documentation
# generation with the widely used Sphinx. This is achieved with the command:
make -C docs html
# What does this do? Basically, we get some nice HTML pages that can readily be
# served as a webpage directly produced from two sources:
# 1. Some reStructuredText files that live in the docs/ folder
# 2. The dosctrings of our source code. 

# We will see what this looks like now by moving to another useful feature of
# the template: continuous integration.
#
# (In web browser, navigate to https://gitlab.com/mbluteau/example/ or switch to
# the tab that was used when creating the project above and refresh)
#
# On main repo page, there should a be a green tick next to our most recent
# commit, signifying that the CI/CD pipeline has run successfully. Clicking on
# that, we should see the jobs that actually ran within this pipeline. There is
# one for the tests, another to build the docs, and another to deploy
# the docs. The documentation can be viewed by going to "Settings" > "Pages" >
# and then clicking the link under "Access pages"
#
# This was all performed by the `.gitlab-ci.yml` file in the template-generated
# project.
