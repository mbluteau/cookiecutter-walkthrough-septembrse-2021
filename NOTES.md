# Rough Preparatory Notes for SeptembRSE Walkthrough

## Brainstorm

- from a practical standpoint, doing this from a Docker container will be
  advantageous so that I have a truly clean environment where none of my
  existing customisations will interfere. 
  - will need to have python and some editor installed (vim likely)
  - also, will need to get my research scripts that are already under version
    control
    - I can either copy the git repo into the Docker image directly, or clone
      it; cloning probably more transparent.
  - need to have a non-root user when doing all of this

- for accessibility, I want to have a clear, simple, legible prompt, and the
  font of my terminal should also be big enough to see with colour-blind
  friendly colours

- a crucial decision I need to make is whether to do this live or pre-record it
  - TODO consult with conference committee to see if this is an option


- slides
  - put slido link in slides
  - to introduce the subject and provide links to resources
  - use UKAEA template? probably

- demo script
  - what do I actually want to run through and demonstrate?
  - key: I will need to have a remote repo set up to demonstrate the CI
    features of the template
    - probably go with GitLab(.com?) since it is easier
    - how will I get ssh keys into the Docker container?
  - probably the best way to generate the script is to start tinkering with the
    container and then simply copy and paste the commands I use from history

## Slide Outline

- the problem: lack of good "standard" practices in research software
  - rewatching Graham Lee's "Clowntown" presentation, we don't necessarily want
  to emulate industry, but I think we can all agree there is a baseline set of
  practices that provide demonstrable benefit to software projects
- what is `cookiecutter` and how can it help

